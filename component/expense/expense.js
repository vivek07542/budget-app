import React, { useRef, useEffect, useContext } from 'react';
import Card from '../ui/Card.js';
import AppContext from '../../provider/contextApi';
const Expense = () => {
  const {
    editMode,
    setEditMode,
    editObject,
    setEditObject,
    expenseItem,
    setExpenseItem,
    setExpenditure,
  } = useContext(AppContext);

  const price = useRef('');
  const item = useRef('');

  useEffect(() => {
    if (editMode && editObject !== null) {
      price.current.value = editObject.price;
      item.current.value = editObject.item;
    }
  }, [editMode, editObject]);
  const submitHandler = (e) => {
    e.preventDefault();
    const eachItem = {
      id: !editMode
        ? expenseItem.length === 0
          ? expenseItem.length + 1
          : expenseItem[expenseItem.length - 1].id + 1
        : editObject !== null && editObject.id,
      item: item.current.value,
      price: +price.current.value,
    };
    editMode
      ? (expenseItem[expenseItem.findIndex((x) => x.id == eachItem.id)] =
          eachItem)
      : expenseItem.splice(eachItem.id - 1, 0, eachItem);
    const fetchPrice = expenseItem.map((x) => x.price).reduce((a, c) => a + c);
    setExpenditure(fetchPrice);
    setExpenseItem([...expenseItem]);
    editMode && setEditObject(null);
    editMode && setEditMode(false);
    item.current.value = '';
    price.current.value = '';
  };
  return (
    <div className="flex flex-row">
      <div className="basis-1/4">
        <Card>
          <form onSubmit={submitHandler}>
            <div className="flex flex-col gap-3 content-around">
              <input
                className="w-60 h-7 pl-1"
                required
                type="text"
                placeholder="Item"
                id="item"
                ref={item}
              />
              <input
                className="w-60 h-7 pl-1"
                required
                type="number"
                placeholder="Price"
                id="price"
                ref={price}
              />
              <button
                type="submit"
                className="w-20 mx-auto text-sm bg-red-300 rounded-sm p-1 hover:bg-red-600 hover:text-white "
              >
                {' '}
                Add
              </button>
            </div>
          </form>
        </Card>
      </div>
    </div>
  );
};

export default Expense;
