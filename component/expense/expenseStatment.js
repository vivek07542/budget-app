import React, { useContext } from 'react';
import { AiOutlineEdit, AiOutlineDelete } from 'react-icons/ai';
import AppContext from '../../provider/contextApi';
const ExpenseStatment = () => {
  const {
    expenseItem,
    setEditMode,
    setEditObject,
    editMode,
    editObject,
    setExpenseItem,
    setExpenditure,
  } = useContext(AppContext);

  const editHandler = (item) => {
    setEditMode(true);
    setEditObject(item);
  };
  const deleteHandler = (index) => {
    expenseItem.splice(index, 1);
    const fetchPrice = expenseItem.map((x) => x.price).reduce((a, c) => a + c);
    setExpenditure(fetchPrice);
    setExpenseItem([...expenseItem]);
  };
  return (
    <div className=" my-4 py-3  ">
      <h2 className="text-center bold text-lg text-purple-400 my-2">
        Expense Detail
      </h2>
      <table className="mx-auto min-w-[50%] ">
        <thead>
          <tr>
            <th>Sr.No.</th>
            <th>Item</th>
            <th>Price</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody className="text-center">
          {expenseItem.map((e, index) => (
            <tr
              key={index}
              className={
                editMode && editObject.id === e.id ? 'bg-slate-200' : undefined
              }
            >
              <td>{e.id}</td>
              <td>{e.item}</td>
              <td>{e.price}</td>
              <td>
                <div className="flex flex-row justify-evenly">
                  <button
                    className="text-green-800 hover:text-red-600"
                    onClick={() => editHandler(e)}
                  >
                    <AiOutlineEdit />
                  </button>
                  <button
                    className="text-green-800 hover:text-red-600"
                    onClick={() => deleteHandler(index)}
                  >
                    <AiOutlineDelete />
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ExpenseStatment;
