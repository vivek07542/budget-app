import React, { useRef, useContext } from 'react';
import BudgetStatment from './budgetStatment.js';
import AppContext from '../../provider/contextApi';
import Card from '../ui/Card.js';
const Budget = () => {
  const { setBudget } = useContext(AppContext);
  const allocateBudget = useRef('');
  const submitHandler = (e) => {
    e.preventDefault();
    setBudget(allocateBudget.current.value);
    allocateBudget.current.value = '';
  };
  return (
    <div className="flex flex-row ">
      <div className=" basis-1/4 m-2">
        <Card>
          <form onSubmit={submitHandler}>
            <div className="flex justify-between">
              <input
                type="number"
                placeholder="Allocate Budget"
                id="allocateBudget"
                ref={allocateBudget}
              />
              <button
                type="submit"
                className="text-sm bg-red-300 rounded-sm p-1 hover:bg-red-600 hover:text-white"
              >
                {' '}
                Approved{' '}
              </button>
            </div>
          </form>
        </Card>
      </div>
      <div className="basis-3/4 m-2 px-6 py-4">
        <BudgetStatment />
      </div>
    </div>
  );
};

export default Budget;
