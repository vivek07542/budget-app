import React, { useContext } from 'react';
import AppContext from '../../provider/contextApi';

const BudgetStatment = () => {
  const { budget, expenditure } = useContext(AppContext);

  return (
    <div>
      <div className="flex justify-around">
        <p className="text-lg bold ">
          Total Budget :<span className="text-green-600"> $ {budget}</span>{' '}
        </p>
        <p className="text-lg bold ">
          Total Expenditure :
          <span className="text-red-600"> $ {expenditure}</span>
        </p>
        <p className="text-lg bold ">
          Bal. Amount :{' '}
          <span className="text-purple-600"> $ {budget - expenditure}</span>{' '}
        </p>
      </div>
      {expenditure > budget && (
        <p className="text-sm text-red-600">Budget Exceed !!</p>
      )}
    </div>
  );
};

export default BudgetStatment;
