import React from 'react';

const Card = (props) => {
  return (
    <div className="max-w rounded overflow-hidden shadow-lg">
      <div className="px-5 py-4">{props.children}</div>
    </div>
  );
};

export default Card;
