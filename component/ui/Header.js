const Header = () => {
  return (
    <header>
      <nav className="bg-green-300">
        <p className="text-2xl text-green-600 px-4 py-1">Budget App</p>
      </nav>
    </header>
  );
};
export default Header;
