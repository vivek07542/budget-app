import { useState, createContext } from 'react';
import '../styles/globals.css';
import AppContext from '../provider/contextApi';

function MyApp({ Component, pageProps }) {
  const [budget, setBudget] = useState(0);
  const [editMode, setEditMode] = useState(false);
  const [expenditure, setExpenditure] = useState(0);
  const [editObject, setEditObject] = useState(null);
  const [expenseItem, setExpenseItem] = useState([]);

  return (
    <AppContext.Provider
      value={{
        budget,
        setBudget,
        editObject,
        setEditObject,
        expenditure,
        setExpenditure,
        expenseItem,
        setExpenseItem,
        editMode,
        setEditMode,
      }}
    >
      <Component {...pageProps} />
    </AppContext.Provider>
  );
}

export default MyApp;
